//
//  Series.swift
//  TvTime
//
//  Created by Ciro Rosati on 06/12/2019.
//  Copyright © 2019 Ciro Rosati. All rights reserved.
//

import UIKit

class Series: UITableViewCell {
    
    
    //   MARK --- @IBOUTLET
    
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var seriesImage: UIImageView! {
        didSet {
            seriesImage.layer.cornerRadius = 3
        }
    }
    @IBOutlet weak var roundedView: UIView! {
        didSet {
            roundedView.layer.cornerRadius = 3
            roundedView.layer.masksToBounds = true
            roundedView.layer.backgroundColor = UIColor.white.cgColor
            roundedView.layer.shadowOffset = CGSize(width: 0, height: 0)
            roundedView.layer.shadowColor = UIColor.black.cgColor
            roundedView.layer.shadowRadius = 5
            roundedView.layer.shadowOpacity = 0.20
            roundedView.layer.masksToBounds = false;
            roundedView.clipsToBounds = false;
        }
    }
    @IBOutlet weak var episodesTitle: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    func setup(_ item: SeriesCategory) {
        episodesTitle.text = item.name
        titleLabel.text = item.titleLabel
        titleLabel.layer.cornerRadius = 12
        titleLabel.layer.borderWidth = 0.8
        titleLabel.sizeToFit()
        titleLabel.frame = CGRect(x: titleLabel.frame.origin.x, y: titleLabel.frame.origin.y, width: titleLabel.frame.width+24, height: titleLabel.frame.height+10)
        //        titleLabel.layer.borderWidth = titleLabel.frame.width+15
        titleLabel.layer.masksToBounds = true
        if let image = UIImage(named: item.image) {
            seriesImage.image = image
        }
        checkButton.imageView?.image = item.buttonState.status
        subtitleLabel.text = item.subLabel
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
        @IBAction func buttonTapped (_ sender: UIButton){
        if (checkButton.isSelected == true){
            sender.setImage(UIImage(named: "greenCheck_Normal"), for: .selected)
        }
    }
}
 


