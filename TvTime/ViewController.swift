//
//  ViewController.swift
//  TvTime
//
//  Created by Ciro Rosati on 05/12/2019.
//  Copyright © 2019 Ciro Rosati. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //  Mark ----  @IBOutlet
    
    @IBOutlet weak var tableView: UITableView!
    
    //  Mark ---- var

    var sections = [[SeriesCategory]]()
    var list: [SeriesCategory] = []
    var visti: [SeriesCategory] = []
    var daVedere: [SeriesCategory] = []
    var daVedereDaTempo: [SeriesCategory] = []
    var nonIniziato: [SeriesCategory] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Image")!)
        let codeSegmented = CustomSegmentedControl(frame: CGRect(x: 0, y: 45, width: self.view.frame.width
            , height: 30), buttonTitle: ["LISTA DA VEDERE","IN ARRIVO"])
        codeSegmented.backgroundColor = .clear
        view.addSubview(codeSegmented)
        
        visti = createVisti()
        daVedere = createDaVedere()
        daVedereDaTempo = createNonVistiDaTempo()
        nonIniziato = createNonIniziato()
        sections = [visti, daVedere, daVedereDaTempo, nonIniziato]
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = .clear
        
        // Do any additional setup after loading the view.
        
        tableView.setContentOffset( CGPoint(x: 0, y: tableView.frame.height/3), animated: false)
    }
}

func createVisti() -> [SeriesCategory]{
    var tempEpisodes: [SeriesCategory] = []
    
    tempEpisodes.append(SeriesCategory(image: "PeakyBlinders", name: "S05 | 04", titleLabel: "PEAKY BLINDERS  >", subLabel: "Il Cappio", buttonState: .seen))
    tempEpisodes.append(SeriesCategory(image: "PeakyBlinders", name: "S05 | 05", titleLabel: "PEAKY BLINDERS  >", subLabel: "Lo Shock", buttonState: .seen))
    tempEpisodes.append(SeriesCategory(image: "sup-1", name: "S15 | E01", titleLabel: "SUPERNATURAL  >", subLabel: "Back and to the Future", buttonState: .seen))
    tempEpisodes.append(SeriesCategory(image: "sup-1", name: "S15 | E02", titleLabel: "SUPERNATURAL  >", subLabel: "Raising Hell", buttonState: .seen))
    tempEpisodes.append(SeriesCategory(image: "sup-1", name: "S015 | 03", titleLabel: "SUPERNATURAL  >", subLabel: "The Rupture", buttonState: .seen))
    return tempEpisodes
}

func createDaVedere() -> [SeriesCategory]{
    var tempEpisodes2 : [SeriesCategory] = []
    let episode3 = SeriesCategory(image: "sup-1", name: "S015 | 04", titleLabel: "SUPERNATURAL  >", subLabel: "Atomic Monster", buttonState: .notSeen)
    tempEpisodes2.append(episode3)
    return tempEpisodes2
}

func createNonVistiDaTempo() -> [SeriesCategory]{
    var tempEpisodes3 : [SeriesCategory] = []
        let episode4 = SeriesCategory(image: "UmbrellAcademy", name: "S01 | 02", titleLabel: "THE UMBRELLA ACADEMY  >", subLabel: "Run Boy Run", buttonState: .notSeen)
    let episode5 = SeriesCategory(image: "AmericanGods", name: "S01 | 05", titleLabel: "AMERICAN GODS  >", subLabel: "The ways of the Dead", buttonState: .notSeen)
    let episode6 = SeriesCategory(image: "BreakingBad", name: "S02 | 01", titleLabel: "BREAKING BAD  >", subLabel: "Tutto Cambia", buttonState: .notSeen)
    let episode7 = SeriesCategory(image: "13", name: "S02 | 02", titleLabel: "TREDICI  >", subLabel: "Due ragazze e un bacio", buttonState: .notSeen)
    
    tempEpisodes3.append(episode4)
    tempEpisodes3.append(episode5)
    tempEpisodes3.append(episode6)
    tempEpisodes3.append(episode7)
    
    
    return tempEpisodes3
}

func createNonIniziato() -> [SeriesCategory]{
    var tempEpisodes4 : [SeriesCategory] = []
    
    let episode8 = SeriesCategory(image: "Suburra", name: "S01 | 01", titleLabel: "SUBURRA - LA SERIE  >", subLabel: "21 giorni", buttonState: .notSeen)
    let episode9 = SeriesCategory(image: "Vikings", name: "S01 | 01", titleLabel: "VIKINGS  >", subLabel: "Riti di  passaggio", buttonState: .notSeen)
    let episode10 = SeriesCategory(image: "Ozark", name: "S01 | 01", titleLabel: "OZARK  >", subLabel: "Il mio bon bon", buttonState: .notSeen)
    let episode11 = SeriesCategory(image: "GoT", name: "S01 | 01", titleLabel: "GAME OF THRONES  >", subLabel: "L'inverno sta arrivando", buttonState: .notSeen)
    
    tempEpisodes4.append(episode8)
    tempEpisodes4.append(episode9)
    tempEpisodes4.append(episode10)
    tempEpisodes4.append(episode11)
    
    
    return tempEpisodes4
}

//MARK: - UITableViewDelegate
extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

//MARK: - UITableViewDataSource


extension ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].count
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: tableView.frame.height))
        headerView.backgroundColor = .clear
        headerView.center.x = tableView.center.x
        let label = UILabel(frame: CGRect(x: 95, y: 12.5, width: tableView.frame.width-200, height: 25))
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 14, weight: .medium)
        label.textColor = UIColor(named: "HeaderText")
        label.textAlignment = .center
        label.layer.cornerRadius = label.frame.height/2
        label.layer.masksToBounds = true
        label.backgroundColor = UIColor(named: "HeaderBackground")
        
        switch (viewForHeaderInSection) {
        case 0:
            label.text = "EPISODI VISTI";
        case 1:
            label.text = "GUARDA IL PROSSIMO";
        case 2:
            label.text = "NON VISTI DA TEMPO";
        case 3:
            label.text = "NON INIZIATO";

            
        default:
            label.text = "tu";
        }
        headerView.addSubview(label)
        return headerView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier:
            Constants.tipReusableCellIdentifier, for: indexPath)
            as? Series else { return UITableViewCell() }
        
        let item = sections[indexPath.section][indexPath.row]
        cell.setup(item)
        self.tableView.separatorInset = UIEdgeInsets.zero
        self.tableView.layoutMargins = UIEdgeInsets.zero
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let closeAction = UIContextualAction(style: .normal, title:  "Seen", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
        })
        closeAction.backgroundColor = UIColor(named: "swipeColor")
        return UISwipeActionsConfiguration(actions: [closeAction])
    }
    
}  
