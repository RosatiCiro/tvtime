//
//  seriesCategory.swift
//  TvTime
//
//  Created by Ciro Rosati on 06/12/2019.
//  Copyright © 2019 Ciro Rosati. All rights reserved.
//
//
import Foundation
import UIKit

enum SeriesStatus {
    
    case seen
    case notSeen
    
    var status: UIImage? {
        switch self {
        case .seen: return UIImage(named: "greenCheck_Normal")
        case .notSeen: return UIImage(named: "grayCheck_Normal")
        }
    }
}

struct SeriesCategory {
    var image: String
    var name: String
    var titleLabel: String
    var subLabel: String
    var buttonState: SeriesStatus
}
